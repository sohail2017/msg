<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Message;
use Auth;

class MsgController extends Controller
{
    public function home(Request $request)
    {
        $slack = new SlackController();
        $response = $slack->postMessage('aaaasending form app1', '#alert');
        
    }

    public function chat(Request $request)
    {
        $slack = new SlackController();
        $response = $slack->getList();
        $users = json_decode($response, 1);
        if(!empty($users))
        {
            $users = $users['members'];
        }
        return view('chat', compact('users'));
    }

    public function send(Request $request)
    {
        $to = $request->to;
        $msg = $request->msg;
        $slack = new SlackController();
        $response = $slack->postMessage($msg, $to);
        return ['success' => true];
    }

    public function fetch(Request $request)
    {
        try
        {
            $to = $request->to;
            $msg = \DB::select(\DB::raw("SELECT * FROM messages WHERE (`from` = '".Auth::user()->slack_id."' and `to` = '".$to."') OR (`to` = '".Auth::user()->slack_id."' and `from` = '".$to."') "));
            $messages = json_decode(json_encode($msg), 1);

            $channel_id = null;
            if(count($messages) > 0)
            {
                $channel_id = $messages[0]['channel_id'];
            }

            return ['success' => true, 'channel_id' => $channel_id, 'messages' => $messages];
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'Something went wrong!'];
        }
    }

    public function poll(Request $request)
    {
        try
        {
            $to = $request->to;
            if(empty($to))
            {
                return ['success' => false, 'msg' => 'Please provide to'];
            }
            $msg = \DB::select(\DB::raw("SELECT * FROM messages WHERE (`from` = '".Auth::user()->slack_id."' and `to` = '".$to."') OR (`to` = '".Auth::user()->slack_id."' and `from` = '".$to."') "));
            $msg = json_decode(json_encode($msg), 1);
            $channel_id = 0;
            foreach ($msg as $key => $value) {
                $channel_id = $value['channel_id'];
            }

            return ['success' => true, 'channel_id' => $channel_id, 'messages' => $msg];
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'Something went wrong!'];
        }
    }
}
