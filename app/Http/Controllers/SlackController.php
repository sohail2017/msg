<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\User;
use Auth;
use App\Message;

class SlackController extends Controller
{

    protected $token;
    protected $slackURL;
    protected $client_id;
    protected $client_secret;
    public function __construct()
    {
        $this->client_id = '2452646660832.2438483422196';
        $this->client_secret = 'cd9ff4b5f5da4ddf6b505b293e4c81b9';
        $this->slackURL = 'https://slack.com/api/';
    }

    public function postMessage($text, $to)
    {
        try {
            $data = [
                'text' => $text,
                'channel' => $to,
                'api' => 'chat.postMessage'
            ];
            $api_data = $this->hitAPI($data);
            if($api_data)
            {
                $response = json_decode($api_data, 1);
                if($response['ok'])
                {
                    Message::create([
                        'from' => Auth::user()->slack_id,
                        'to' => $to,
                        'message' => $text,
                        'message_id' => $response['message']['bot_id'],
                        'channel_id' => $response['channel']
                    ]);
                }
            }
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'Something went wrong!'];
        }
    }


    public function hitAPI($data)
    {
        try {
            /* Init cURL resource */
            $content_type = 'application/json';
            $curl = curl_init();
            if(array_key_exists('channel', $data) && array_key_exists('text', $data))
            {
                $post_data = '{
                    "channel": "'.$data['channel'].'",
                    "text" : "'.$data['text'].'",
                    "token": "'.Auth::user()->access_token.'"
                }';
            }
            elseif(array_key_exists('channel', $data))
            {
                $post_data = 'channel='.$data['channel'].'&token='.Auth::user()->access_token;

                $content_type = 'application/x-www-form-urlencoded';
            }
            else
            {
                $post_data = '{
                    "token": "'.Auth::user()->access_token.'"
                }';
            }
            
            $headers = [];
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->slackURL.$data['api'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $post_data,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer '.Auth::user()->access_token,
                    'Content-Type: '.$content_type
                ),
            ));
            $result = curl_exec($curl);
            return $result;

        } catch (Exception $e) {
            return ['success' => false, 'message' => 'Something went wrong!'];
        }
    }

    public function slack_callback(Request $request)
    {
        $code = $request->code;

        /* Init cURL resource */
        $curl = curl_init();

        /* set the content type json */

        $headers = [];
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://slack.com/api/oauth.v2.access',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'client_id='.$this->client_id.'&client_secret='.$this->client_secret.'&code='.$code,
            CURLOPT_HTTPHEADER => array(
                'Content-Type:application/x-www-form-urlencoded'
            ),
        ));
        $result = curl_exec($curl);
        $result = json_decode($result, 1);
        if($result['ok'] && isset($result['authed_user']))
        {
            $token = $result['authed_user']['access_token'];
            $user = User::where('slack_id', $result['authed_user']['id'])->first();
            if(!empty($user))
            {
                User::where('slack_id', $result['authed_user']['id'])->update(['access_token' => $token]);
            }
            else
            {
                $user = User::create([
                    'name' => $result['authed_user']['id'],
                    'email' => $result['authed_user']['id']."@test.com",
                    'slack_id' => $result['authed_user']['id'],
                    'access_token' => $token,
                    'password' => bcrypt('123456')
                ]);
            }

            \Auth::loginUsingId($user->id);

            return redirect('chat');
        }
    }

    public function getList()
    {
        try {
            $data = [
                'api' => 'users.list'
            ];
            return $this->hitAPI($data);
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'Something went wrong!'];
        }
    }

    public function getMessages($to)
    {
        try {
            $data = [
                'api' => 'conversations.history',
                'channel' => $to
            ];
            return $this->hitAPI($data);
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'Something went wrong!'];
        }
    }

    public function events(Request $request)
    {
        $event = file_get_contents("php://input"); 
        $eventArray = json_decode($event, TRUE);
        error_log("\n\r event hook: ".$event." <POST>". print_r($request->all(), 1), 3, "sohail.txt");
        $message = [];
        $event = $request->all();
        if(!empty($request['event']) && !empty($request['event']['type']) && $request['event']['type'] == 'message')
        {
            $message = [
                'message' => $request['event']['text'],
                'from' => $request['event']['user'],
                'to' => $request['authorizations'][0]['user_id'],
                'message_id' => $request['event']['client_msg_id'],
                'channel_id' => $request['event']['channel']
            ];
            Message::create($message);
        }
    }
}
