<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            img.p-image {
                border-radius: 50%;
                float: left;
            }
            li {
                padding: 10px;
                border: 1px solid;
                list-style-type: none;
                cursor: pointer;
            }
            span.p-name {
                font-size: 20px;
                font-weight: 600;
                color: #719696;
            }
            .p-msg-bar {
                position: sticky;
                bottom: 0px;
                display: flex;
                top: 100%;
            }
            .box{
                border: 1px solid;
            }
            .p-chat {
                border: 1px solid;
                max-height: 200px;
                height: 200px;
                overflow-y: auto;
            }
            .your-msg {
                float: left;
                padding: 5px 10px;
                border: 1px;
                border-radius: 22px 22px 22px 0px;
                background: #5d5d8c;
                color: white;
                margin: 10px 20px;
                display: block;
                width: 50%;
                text-align: left;
            }

            .my-msg {
                float: right;
                padding: 5px 10px;
                /* border: 1px; */
                border-radius: 22px 22px 0px 22px;
                background: #e1e1ec;
                color: gray;
                margin: 10px 20px;
                width: 50%;
                text-align: left;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">
    </head>
    <body>
        <div class="content">
            <div class="title m-b-md">
                Messages
            </div>

            <div class="col-md-12" style="display: flex;">
                <div class="col-md-3 pull-left">
                    <h1>List</h1>
                    <ul>
                        @foreach($users as $user)
                            @if(!$user['is_bot'] && $user['id'] != Auth::user()->slack_id)
                                <li class="profile" data-id="{{$user['id']}}" data-name="{{$user['real_name']}}">
                                    <img class="p-image" src="{{$user['profile']['image_32']}}" />
                                    <span class="p-name"> {{$user['real_name']}} </span>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-9 pull-right" style="display:flex;">
                    <div class="box offset-md-2 col-md-6">
                        <div class="p-title"></div>
                        <div class="p-chat"></div>
                        <div class="p-msg-bar">
                            <input type="text" name="msg" style="width: 400px;margin: 5px;">
                            <input type="hidden" name="to">
                            <button class="p-send btn btn-success" style="margin: 5px;">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var user_id = 0;
            setInterval(function(){
                poll(user_id);
            }, 3000);
            var user_id = 0;
            $(document).on('click', '.profile', function(){
                user_id = $(this).attr('data-id');
                $('.box').find('.p-title').text($(this).attr('data-name'));
                $('.p-chat').html("");
                fetchMessages(user_id);
            });

            $(document).on('click', '.p-send', function(){
                to = user_id;
                msg = $('input[name="msg"]').val();
                $('input[name="msg"]').val("");
                $('.p-chat').append('<div class="my-msg">'+ msg +'</div>');
                $.ajax({
                    url: "{{url('/send')}}",
                    type: 'post',
                    data: {to: to, msg: msg, "_token": "{{ csrf_token() }}"},
                    success: function(response){

                    },
                    error: function(error){

                    }
                })
            });
        });

        function fetchMessages(user_id) {
            $.ajax({
                url: "{{url('/fetch')}}",
                data: {to: user_id},
                type: 'get',
                success: function(response){
                    if(response.success){
                        channel_id = response.channel_id;
                        $.each(response.messages, function(key, value){
                            if(value.from == '{{Auth::user()->slack_id}}'){
                                $('.p-chat').append('<div class="my-msg">'+ value.message +'</div>');
                            }else{
                                $('.p-chat').append('<div class="your-msg">'+ value.message +'</div>');
                            }
                        });
                    }
                },
                error: function(error){

                }
            });
        }

        function poll(user_id) {
            $.ajax({
                url: "{{url('/poll')}}",
                data: {to: user_id},
                type: 'get',
                success: function(response){
                    if(response.success){
                        $('.p-chat').html("");
                        
                        channel_id = response.channel_id;
                        $.each(response.messages, function(key, value){
                            if(value.from == '{{Auth::user()->slack_id}}'){
                                $('.p-chat').append('<div class="my-msg">'+ value.message +'</div>');
                            }else{
                                $('.p-chat').append('<div class="your-msg">'+ value.message +'</div>');
                            }
                        });
                        $('.p-chat').animate({
                            scrollTop: $('.p-chat')[0].scrollHeight}
                        , "slow");
                    }
                },
                error: function(error){

                }
            });
        }
    </script>
</html>
