<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::any('slack_callback', 'SlackController@slack_callback');
Route::any('redirect_uri', 'SlackController@redirect_uri');
// Route::any('events', 'SlackController@events');

Route::get('/', function () {
    if(\Auth::user()){
        return redirect('chat');
    }
    return view('welcome');
});

Route::get('home', 'MsgController@home');
Route::get('chat', 'MsgController@chat');
Route::get('fetch', 'MsgController@fetch');
Route::get('poll', 'MsgController@poll');
Route::post('send', 'MsgController@send');
